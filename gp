#!/bin/bash


function vstack() {
    ffmpeg -i $1 -i $2 -filter_complex vstack $3
}

function grid4() {
    ffmpeg -i $1 -i $2 -i $3 -i $4 -filter_complex "[0:v][1:v]hstack[top];[2:v][3:v]hstack[bottom];[top][bottom]vstack[v]" -map "[v]" $5
}

function trim(){
    ffmpeg -i $1 -ss $3 -t $4 -q:a 1 -q:v 1 -vcodec libx264 $2
}

function convert(){
	if [[ $1 = "" ]]; then
		for i in *.MP4;
		    do name=`echo $i | cut -d'.' -f1`;
		    echo $name;
		    ffmpeg -i $i -q:a 1 -q:v 1 -vcodec mpeg4 $name.mov;
		done
	else
		name=`echo $1 | cut -d'.' -f1`;
	   	echo $name;
	    	ffmpeg -i $1 -q:a 1 -q:v 1 -vcodec mpeg4 $name.mov;
	fi
}

function timecode() {
    ffmpeg -i $1 -vf "drawtext=fontfile=./private/DejaVuSans.ttf: timecode='01\:00\:00\:00': r=25: x=(w-tw)/2: y=h-(2*lh): fontcolor=white: box=1: boxcolor=0x00000099" -y $2
}

function framecode() {
    ffmpeg -i $1 -vf "drawtext=fontfile=./private/DejaVuSans.ttf: text=%{n}: x=(w-tw)/2: y=h-(2*lh): fontcolor=white: box=1: boxcolor=0x00000099" -y $2
}

function help() {
    echo 'GoPro Tool for Myself
Available commands: 
- gopro vstac [input1] [input2] [output]
	>>Vertically stack 2 identical sized and lengthed videos
	>>Example: gp vstack input1.mp4 input4.mp4 out-stacked.mp4

- gopro convert
	>>Converts all GoPro MP4 videos to MPEG4 MOV videos for easy editing

- gopro trim [input video] [output video] [HH:MM:SS start] [HH:MM:SS stop]
	>>Trims a video, use this to trim a slow motion video!
	>>Example: gopro trim GOPR0553.MP4 Trimmed.mp4 00:05:04 00:07:43

- gp help
	shows this message
'
}



echo "GoPro Tool for myself"
echo "To see a list of commands and syntax available run: gp help"
echo "Checking dependencies..."
hash ffmpeg 2> /dev/null || { echo >&2 "ffmpeg ..... Not installed!";}

$@
