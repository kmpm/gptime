import functools
import json

from flask_mqtt import Mqtt
from .camera import Gp


def init_app(app, socketio):
    mqtt = Mqtt(app)
    gp = Gp()

    @mqtt.on_connect()
    def handle_connect(a, b, c, d):
        mqtt.subscribe('gptime/camera/shutter')
        mqtt.subscribe('gptime/camera/state')

    @socketio.on('publish')
    def handle_publish(json_str):
        data = json.loads(json_str)
        mqtt.publish(data['topic'], data['message'])


    @socketio.on('subscribe')
    def handle_subscribe(json_str):
        data = json.loads(json_str)
        mqtt.subscribe(data['topic'])


    @mqtt.on_message()
    def handle_mqtt_message(client, userdata, message):
        data = dict(
            topic=message.topic,
            payload=message.payload.decode()
        )
        if message.topic == 'gptime/camera/state':
            print('correct topic', data['payload'])
            if data['payload'] == 'connect':
                gp.connect()

        socketio.emit('mqtt_message', data=data)


    @mqtt.on_log()
    def handle_logging(client, userdata, level, buf):
        print(level, buf)