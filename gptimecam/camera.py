from goprocam import GoProCamera, constants
import time

__all__ = ['Gp']


class Gp:
    def __init__(self):
        pass

    def connect(self):
        self.gpCam = GoProCamera.GoPro()
        self.gpCam.syncTime()
        self.gpCam.video_settings("720p","120")
        self.gpCam.gpControlSet(constants.Video.PROTUNE_VIDEO, constants.Video.ProTune.ON)

    def start(self):
        self.gpCam.shutter(constants.start)

    def stop(self):
        self.gpCam.shutter(constants.stop)
    

    def getLast(self, filename):
        self.gpCam.downloadLastMedia(custom_filename=filename)