gptime
======


# Developing
Rasbian "Stretch" uses Python 3.5.3 so try to target that as far as that part goes.

On Mac you might have to fix some tls issues after installing python 3.5.3
```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
```




# Tools
* https://github.com/KonradIT/gopro-linux

# Converting

info about the file `ffmpeg -i input.MP4  -hide_banner`

## https://video.stackexchange.com/questions/20750/how-do-you-load-gopro-footage-into-blackmagic-fusion

```bash
ffmpeg -i input.mp4 -c:v dnxhd -b 440M -c:a copy out.mov
```


# Combining

## https://superuser.com/questions/153160/join-videos-split-screen

```bash
ffmpeg -i input1.mp4 -i input2.mp4 -filter_complex \
'[0:v]pad=iw*2:ih[int];[int][1:v]overlay=W/2:0[vid]' \
-map [vid] -c:v libx264 -crf 23 -preset veryfast output.mp4
```


## https://unix.stackexchange.com/questions/233832/merge-two-video-clips-into-one-placing-them-next-to-each-other

```bash
ffmpeg \
  -i input1.mp4 \
  -i input2.mp4 \
  -filter_complex '[0:v]pad=iw*2:ih[int];[int][1:v]overlay=W/2:0[vid]' \
  -map [vid] \
  -c:v libx264 \
  -crf 23 \
  -preset veryfast \
  output.mp4
```

## Stacked
https://stackoverflow.com/questions/11552565/vertically-or-horizontally-stack-several-videos-using-ffmpeg

```bash
ffmpeg -i top.mov -vf 'pad=iw:2*ih [top]; movie=bottom.mov [bottom]; \
  [top][bottom] overlay=0:main_h/2' stacked.mov

# or using vstack
ffmpeg -i input0 -i input1 -filter_complex vstack output



```