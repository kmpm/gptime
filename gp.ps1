Function Test-CommandExists
{
    Param ($command)
    $oldPreference = $ErrorActionPreference
    $ErrorActionPreference = 'stop'
    try {if(Get-Command $command){RETURN $true}}
    Catch { RETURN $false }
    Finally {$ErrorActionPreference=$oldPreference}
} #end function test-CommandExists

function vstack {
    $input1 = $args[0]
    $input2 = $args[1]
    $output = $args[2]
    $arglist = "-hide_banner -i $input1 -i $input2 -filter_complex vstack $output"
    #Write-Host $arglist
    Start-Process ffmpeg.exe -ArgumentList $arglist -Wait -NoNewWindow
}

function help {
    Write-Host @"
GoPro tool for myself

Available commands: 
- gopro vstac [input1] [input2] [output]
    >>Vertically stack 2 identical sized and lengthed videos
    >>Example: gp vstack input1.mp4 input4.mp4 out-stacked.mp4    
"@
}

# &"$($args[0])"

if(Test-CommandExists $args[0]){
    $arglist=''
    if ($args.Length -gt 0) {
        $arglist = [System.Collections.ArrayList]$args
        $end = $args.Length - 1
        $arglist = $arglist[1..$end]
    }
    &"$($args[0])" @arglist
} else {
    help
}