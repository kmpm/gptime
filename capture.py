from goprocam import GoProCamera, constants
import time
print('start')
gpCam = GoProCamera.GoPro()
print('stuff')
VIDEO_DURATION=10
gpCam.video_settings("720p","120")
gpCam.gpControlSet(constants.Video.PROTUNE_VIDEO, constants.Video.ProTune.ON)

gpCam.syncTime()

filename="videos/input.mp4"
print("Recording " + str(VIDEO_DURATION) + " seconds video")
gpCam.shutter(constants.start)
time.sleep(VIDEO_DURATION)
# gpCam.downloadLastMedia(gpCam.shoot_video(VIDEO_DURATION), custom_filename=filename)
gpCam.shutter(constants.stop)
time.sleep(2)
gpCam.downloadLastMedia( custom_filename=filename)
print("Downloaded to " + filename)
